import pickle
import socket
from multiprocessing import Queue
from time import time

from raft import Message
from raft.actors.actor import Actor
from raft.messages import Envelope, SupervisorAddress


class SampleActor(Actor):
    def __init__(self, messages):
        super().__init__("testactor")
        self.test_messages = messages

    async def handle_message(self, env: Envelope):
        self.test_messages.put(env)


def send_message(msg, host="localhost", port=9999):
    start_time = time()
    while True:
        try:
            with socket.create_connection((host, port), timeout=1) as sock:
                sock.sendall(pickle.dumps(msg))
        except ConnectionRefusedError:
            # Allow up to 1 second to connect to the socket
            if time() > start_time + 1:
                raise
            continue
        break


def test_run_an_actor(supe):
    """Send a message to actora addressed to actorb. check that it is correctly
    forwarded
    """
    test_messages = Queue()
    actora = SampleActor(test_messages)
    supe.start_actor(actora)
    send_message(
        Envelope(Message(), destination_node="testactor"), port=supe.address.port
    )
    assert isinstance(test_messages.get(timeout=1), Envelope)


class MyMessage(Message):
    pass


class TriggerMessage(Message):
    def __init__(self, port):
        self.port = port


def test_actors_can_communicate(supe, supe2):
    """force one actor to send a message to another through the network
    """

    class ForwardActor(Actor):
        async def handle_message(self, env: Envelope):
            self.send_message(
                Envelope(
                    MyMessage(),
                    destination_node="testactor",
                    destination_addr=SupervisorAddress("localhost", env.msg.port),
                )
            )

    test_messages = Queue()
    test_actor = SampleActor(test_messages)
    forward_actor = ForwardActor("forward")
    supe.start_actor(forward_actor)
    supe2.start_actor(test_actor)
    send_message(
        Envelope(TriggerMessage(supe2.address.port), destination_node="forward"),
        port=supe.address.port,
    )
    assert isinstance(test_messages.get(timeout=1), Envelope)

# pylint: disable=unused-argument
from raft import (
    AppendEntriesMessage,
    AppendEntriesResponse,
    Envelope,
    Follower,
    Leader,
    LogEntry,
    RequestVoteMessage,
    RequestVoteResponse,
)
from raft.cluster_membership import ClusterMembership
from raft.node import Node
from raft.state_machine.candidate import Candidate


def test_reply_false_if_term_lt_current_term(candidate, messages):
    """AppendEntries RPC # 1"""
    candidate.current_term = 4
    msg = AppendEntriesMessage(2, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_reply_false_if_previndex_doesnt_match_prevterm(candidate, messages):
    """AppendEntries RPC # 2"""
    candidate.log.append(LogEntry(96, 5, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_reply_false_if_previndex_doesnt_exist(candidate, messages):
    """AppendEntries RPC # 2"""
    candidate.log.append(LogEntry(92, 2, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_delete_conflict_if_higher_term(candidate, messages):
    """AppendEntries RPC # 3"""
    candidate.log.append(LogEntry(96, 5, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert len(candidate.log) == 0


def test_append_new_entries(candidate, messages):
    """AppendEntries RPC # 4"""
    candidate.log.append(LogEntry(96, 4, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.log[-1].index == 97


def test_append_new_entries_transitions(candidate, messages):
    """Candidate # 3"""
    candidate.log.append(LogEntry(96, 4, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert isinstance(candidate.state, Follower)


def test_leadercommit_gt_commitindex_updates(candidate, messages):
    """AppendEntries RPC # 5"""
    candidate.log.append(LogEntry(96, 4, ""))
    candidate.commit_index = 6
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 96)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.commit_index == 96


def test_commit_min_leadercommit_lastentry(candidate, messages):
    """AppendEntries RPC # 5"""
    candidate.log.append(LogEntry(96, 4, ""))
    candidate.commit_index = 6
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 99)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.commit_index == 97


def test_vote_reply_false_if_term_lt_current_term(candidate, messages):
    """RequestVote RPC # 1"""
    candidate.current_term = 4
    msg = RequestVoteMessage("b", 3, 0, 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_successful_vote(candidate, messages):
    """RequestVote RPC # 2"""
    candidate.current_term = 4
    candidate.voted_for = ""
    msg = RequestVoteMessage("b", 4, 0, 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted


def test_vote_fails_if_voted(candidate, messages):
    """RequestVote RPC # 2"""
    candidate.current_term = 4
    candidate.voted_for = "c"
    msg = RequestVoteMessage("b", 4, 0, 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_vote_fails_if_candidate_not_up_to_date(candidate, messages):
    """RequestVote RPC # 2"""
    candidate.current_term = 4
    candidate.voted_for = ""
    candidate.log.append(LogEntry(1, 1, ""))
    msg = RequestVoteMessage("b", 4, 0, 0)
    response = candidate.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_update_term_convert_to_follower_append_entries(candidate, messages):
    """All Servers # 2"""
    candidate.current_term = 4
    msg = AppendEntriesMessage(5, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.current_term == 5
    assert isinstance(candidate.state, Follower)


def test_update_term_convert_to_follower_ae_response(candidate, messages):
    """All Servers # 2"""
    candidate.current_term = 4
    msg = AppendEntriesResponse(5, 3, True)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.current_term == 5
    assert isinstance(candidate.state, Follower)


def test_update_term_convert_to_follower_vote(candidate, messages):
    """All Servers # 2"""
    candidate.current_term = 4
    msg = RequestVoteMessage("b", 5, 3, 3)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.current_term == 5
    assert isinstance(candidate.state, Follower)


def test_update_term_convert_to_follower_vote_response(candidate, messages):
    """All Servers # 2"""
    candidate.current_term = 4
    msg = RequestVoteResponse(5, True)
    candidate.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert candidate.current_term == 5
    assert isinstance(candidate.state, Follower)


def test_candidate_wins_election(candidate):
    candidate.process_message(
        Envelope(RequestVoteResponse(1, True), source_node="b", destination_node="a")
    )
    assert isinstance(candidate.state, Leader)


def test_candidate_needs_majority(messages):
    """Candidate # 2"""
    node = Node(
        "b", cluster_membership=ClusterMembership(members={"a", "b", "c", "d", "e"})
    )
    node.tick(250)
    node.process_message(
        Envelope(RequestVoteResponse(1, True), source_node="c", destination_node="b")
    )
    assert isinstance(node.state, Candidate)
    node.process_message(
        Envelope(RequestVoteResponse(1, True), source_node="d", destination_node="b")
    )
    assert isinstance(node.state, Leader)


def test_candidate_ignores_false_votes(messages):
    """Candidate # 2"""
    node = Node(
        "b", cluster_membership=ClusterMembership(members={"a", "b", "c", "d", "e"})
    )
    node.tick(250)
    node.process_message(
        Envelope(RequestVoteResponse(1, True), source_node="c", destination_node="b")
    )
    node.process_message(
        Envelope(RequestVoteResponse(1, False), source_node="a", destination_node="b")
    )
    assert isinstance(node.state, Candidate)


def test_candidate_steps_down(candidate):
    candidate.current_term = 2
    response = candidate.process_message(
        Envelope(
            AppendEntriesMessage(2, "c", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="c",
            destination_node="b",
        )
    )
    assert isinstance(candidate.state, Follower)
    assert response.success


def test_candidate_doesnt_step_down_if_lower_term(messages):
    node = Node("b", cluster_membership=ClusterMembership(members={"a", "b", "c"}))
    node.process_message(
        Envelope(
            AppendEntriesMessage(2, "a", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="a",
            destination_node="b",
        )
    )
    node.tick(250)
    response = node.process_message(
        Envelope(
            AppendEntriesMessage(1, "a", 0, 0, [LogEntry(3, 1, "cmd")], 0),
            source_node="a",
            destination_node="b",
        )
    )
    assert isinstance(node.state, Candidate)
    assert response.success is False


def test_candidate_steps_down_if_higher_term(candidate):
    response = candidate.process_message(
        Envelope(
            RequestVoteMessage("b", 5, 1, 1), source_node="b", destination_node="a"
        )
    )
    assert isinstance(candidate.state, Follower)
    assert response.vote_granted


def test_candidate_doesnt_give_vote_twice(candidate):
    response = candidate.process_message(
        Envelope(
            RequestVoteMessage("b", 1, 0, 0), source_node="b", destination_node="a"
        )
    )
    assert isinstance(candidate.state, Candidate)
    assert response.vote_granted is False


def test_candidate_increments_if_timeout_expires(candidate, messages):
    """Candidate # 4"""
    assert messages[0].term == 1
    candidate.tick(250)
    # 0 and 1 should both be term 1, then 2 and 3 should be term 2
    assert messages[2].term == 2

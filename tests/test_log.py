import pytest

from raft import Log, LogEntry


def test_cant_append_out_of_order():
    log = Log()
    log.append(LogEntry(5, 1, ""))
    with pytest.raises(KeyError):
        log.append(LogEntry(3, 1, ""))


def test_can_pop_from_empty_log():
    Log().pop()


def test_can_pop_item():
    log = Log()
    log.append(LogEntry(5, 1, ""))
    log.append(LogEntry(6, 1, ""))
    log.pop()
    assert log[-1].index == 5

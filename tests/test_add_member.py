# pylint: disable=unused-argument
import pytest

from raft import (
    AppendEntriesMessage,
    AppendEntriesResponse,
    Envelope,
    Follower,
    Leader,
    LogEntry,
    RequestVoteResponse,
)
from raft.messages import AddMemberMessage, MembershipResponse


def test_non_member_can_vote(candidate):
    candidate.process_message(
        Envelope(RequestVoteResponse(1, True), source_node="d", destination_node="a")
    )
    candidate.process_message(
        Envelope(RequestVoteResponse(1, True), source_node="e", destination_node="a")
    )
    assert isinstance(candidate.state, Leader)


def test_non_member_can_append(candidate):
    candidate.process_message(
        Envelope(
            AppendEntriesMessage(5, "d", 55, 5, [], 55),
            source_node="d",
            destination_node="a",
        )
    )
    assert isinstance(candidate.state, Follower)


def test_non_leader_responds_to_membership_request(candidate):
    """AddServer / RemoveServer RPC # 1"""
    msg = candidate.process_message(
        Envelope(AddMemberMessage("d"), source_node="d", destination_node="a")
    )
    assert isinstance(msg, MembershipResponse)
    assert msg.status == "NOT_LEADER"


def test_follower_responds_to_membership_request(follower):
    msg = follower.process_message(
        Envelope(AddMemberMessage("d"), source_node="d", destination_node="a")
    )
    assert isinstance(msg, MembershipResponse)
    assert msg.status == "NOT_LEADER"


def test_follower_leader_hint(follower):
    follower.cluster_membership.leader = "c"
    msg = follower.process_message(
        Envelope(AddMemberMessage("d"), source_node="d", destination_node="a")
    )
    assert isinstance(msg, MembershipResponse)
    assert msg.leader_hint == "c"


def test_add_node_sends_catchup(leader, messages):
    messages.empty()
    resp = leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    assert resp is None
    msg = messages[-1]
    assert isinstance(msg, AppendEntriesMessage)
    assert messages.first_for_node("e") is msg


def test_updates_cluster_membership_on_catchup(leader, messages):
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.state.next_index["b"] = 56
    leader.state.next_index["c"] = 56
    leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    messages.empty()
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 55, True), source_node="e", destination_node="a"
        )
    )
    assert messages[-1].entries[0].command.startswith("cluster_membership = {")
    assert all(
        x in messages[-1].entries[0].command for x in ("'a'", "'b'", "'c'", "'e'")
    )


def test_tries_another_round_if_failure(leader, messages):
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    assert leader.cluster_membership.new_servers["e"].rounds == 1
    messages.empty()
    leader.process_message(
        Envelope(
            AppendEntriesResponse(0, 0, False), source_node="e", destination_node="a"
        )
    )
    assert messages.first_for_node("e").entries[0].index == 6
    assert leader.cluster_membership.new_servers["e"].rounds == 2


def test_times_out_after_ten_rounds(leader, messages):
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    leader.cluster_membership.new_servers["e"].rounds = 10
    messages.empty()
    leader.process_message(
        Envelope(
            AppendEntriesResponse(0, 0, False), source_node="e", destination_node="a"
        )
    )
    assert messages.first_for_node("d").status == "TIMEOUT"
    assert len(leader.cluster_membership.members) == 3


def test_can_commit_logs_while_waiting_for_addition(leader, messages):
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 55, True), source_node="b", destination_node="a"
        )
    )
    assert leader.commit_index == 55
    assert len(leader.cluster_membership.members) == 3


def test_responds_ok(leader, messages):
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.state.next_index["b"] = 56
    leader.state.next_index["c"] = 56
    leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    messages.empty()
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 55, True), source_node="e", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="e", destination_node="a"
        )
    )
    # need 3 for quorum now!
    with pytest.raises(IndexError):
        messages.first_for_node("d")
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="b", destination_node="a"
        )
    )
    assert messages.first_for_node("d").status == "OK"


def test_only_add_one_at_a_time(leader, messages):
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.state.next_index["b"] = 56
    leader.state.next_index["c"] = 56
    messages.empty()
    leader.process_message(
        Envelope(AddMemberMessage("e"), source_node="d", destination_node="a")
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 55, True), source_node="e", destination_node="a"
        )
    )
    assert "cluster_membership" in messages.first_for_node("b").entries[0].command
    messages.empty()
    leader.process_message(
        Envelope(AddMemberMessage("z"), source_node="d", destination_node="a")
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="z", destination_node="a"
        )
    )
    # adding E isnt committed yet, so it shouldnt add z yet
    with pytest.raises(IndexError):
        messages.first_for_node("b")
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="e", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="b", destination_node="a"
        )
    )
    assert "'z'" in messages.first_for_node("b").entries[-1].command

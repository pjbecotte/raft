# pylint: disable=unused-argument
from raft import (
    AppendEntriesMessage,
    AppendEntriesResponse,
    Envelope,
    Follower,
    LogEntry,
    RequestVoteMessage,
    RequestVoteResponse,
)
from raft.state_machine.candidate import Candidate


def test_reply_false_if_term_lt_current_term(follower, messages):
    """AppendEntries RPC # 1"""
    follower.current_term = 4
    msg = AppendEntriesMessage(2, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_reply_false_if_previndex_doesnt_match_prevterm(follower, messages):
    """AppendEntries RPC # 2"""
    follower.log.append(LogEntry(96, 5, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_reply_false_if_previndex_doesnt_exist(follower, messages):
    """AppendEntries RPC # 2"""
    follower.log.append(LogEntry(92, 2, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_delete_conflict_if_higher_term(follower, messages):
    """AppendEntries RPC # 3"""
    follower.log.append(LogEntry(96, 5, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert len(follower.log) == 0


def test_append_new_entries(follower, messages):
    """AppendEntries RPC # 4"""
    follower.log.append(LogEntry(96, 4, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.log[-1].index == 97


def test_leadercommit_gt_commitindex_updates(follower, messages):
    """AppendEntries RPC # 5"""
    follower.log.append(LogEntry(96, 4, ""))
    follower.commit_index = 6
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 96)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.commit_index == 96


def test_commit_min_leadercommit_lastentry(follower, messages):
    """AppendEntries RPC # 5"""
    follower.log.append(LogEntry(96, 4, ""))
    follower.commit_index = 6
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 99)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.commit_index == 97


def test_vote_reply_false_if_term_lt_current_term(follower, messages):
    """RequestVote RPC # 1"""
    follower.current_term = 4
    msg = RequestVoteMessage("b", 3, 0, 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_successful_vote(follower, messages):
    """RequestVote RPC # 2"""
    follower.current_term = 4
    follower.voted_for = ""
    msg = RequestVoteMessage("b", 5, 0, 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted


def test_vote_fails_if_voted(follower, messages):
    """RequestVote RPC # 2"""
    follower.current_term = 5
    follower.voted_for = "c"
    msg = RequestVoteMessage("b", 5, 0, 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_vote_fails_if_candidate_not_up_to_date(follower, messages):
    """RequestVote RPC # 2"""
    follower.current_term = 4
    follower.voted_for = ""
    follower.log.append(LogEntry(1, 1, ""))
    msg = RequestVoteMessage("b", 5, 0, 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_update_term_convert_to_follower_append_entries(follower, messages):
    """All Servers # 2"""
    follower.current_term = 4
    msg = AppendEntriesMessage(5, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.current_term == 5
    assert isinstance(follower.state, Follower)


def test_update_term_convert_to_follower_ae_response(follower, messages):
    """All Servers # 2"""
    follower.current_term = 4
    msg = AppendEntriesResponse(5, 3, True)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.current_term == 5
    assert isinstance(follower.state, Follower)


def test_update_term_convert_to_follower_vote(follower, messages):
    """All Servers # 2"""
    follower.current_term = 4
    msg = RequestVoteMessage("b", 5, 3, 3)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.current_term == 5
    assert isinstance(follower.state, Follower)


def test_update_term_convert_to_follower_vote_response(follower, messages):
    """All Servers # 2"""
    follower.current_term = 4
    msg = RequestVoteResponse(5, True)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.current_term == 5
    assert isinstance(follower.state, Follower)


def test_on_startup_transition_to_follower(follower):
    assert isinstance(follower.state, Follower)


def test_on_timeout_transition_to_candidate(messages, follower):
    """Follower # 2"""
    follower.tick(250)
    assert isinstance(follower.state, Candidate)


def test_voting_resets_election_timeout(follower):
    """Follower # 2"""
    # The timeout is random between 100-250. We want to send a vote at 99 to ensure
    # it doesnt roll over, and have enough total time that if it wasnt resetting we
    # would have seen a reset for sure
    follower.tick(99)
    follower.process_message(
        Envelope(
            RequestVoteMessage("b", 1, 6, 2), source_node="b", destination_node="a"
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            RequestVoteMessage("b", 1, 6, 2), source_node="b", destination_node="a"
        )
    )
    follower.tick(99)
    response = follower.process_message(
        Envelope(
            RequestVoteMessage("b", 1, 6, 2), source_node="b", destination_node="a"
        )
    )
    assert response.vote_granted


def test_failed_voting_doesnt_reset_election_timeout(messages, follower):
    """Follower # 2"""
    follower.increment_term()
    follower.tick(99)
    follower.process_message(
        Envelope(
            RequestVoteMessage("b", 0, 6, 2), source_node="b", destination_node="a"
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            RequestVoteMessage("a", 0, 6, 2), source_node="b", destination_node="a"
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            RequestVoteMessage("a", 0, 6, 2), source_node="b", destination_node="a"
        )
    )
    assert isinstance(follower.state, Candidate)


def test_append_entries_resets_election_timeout(follower):
    """Follower # 2"""
    follower.tick(99)
    follower.process_message(
        Envelope(
            AppendEntriesMessage(1, "b", 0, 0, [LogEntry(1, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            AppendEntriesMessage(1, "b", 1, 1, [LogEntry(2, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(99)
    response = follower.process_message(
        Envelope(
            AppendEntriesMessage(1, "b", 2, 1, [LogEntry(3, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    assert response.success


def test_expired_leader_doesnt_reset_election_timeout(messages, follower):
    """Follower # 2"""
    follower.current_term = 5
    follower.process_message(
        Envelope(
            AppendEntriesMessage(1, "b", 0, 0, [LogEntry(1, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            AppendEntriesMessage(1, "b", 0, 1, [LogEntry(0, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            AppendEntriesMessage(1, "b", 0, 1, [LogEntry(0, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(80)
    assert isinstance(follower.state, Candidate)


def test_inconsistent_log_does_reset_election_timeout(follower):
    """Follower # 2"""
    follower.current_term = 2
    follower.log.append(LogEntry(45, 1, ""))
    follower.log.append(LogEntry(50, 1, ""))
    follower.log.append(LogEntry(51, 1, ""))
    follower.log.append(LogEntry(53, 2, ""))
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 53, 1, [LogEntry(54, 1, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 50, 1, [LogEntry(60, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(99)
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 50, 0, [LogEntry(58, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.tick(80)
    assert isinstance(follower.state, Follower)


def test_candidate_increments_term(follower, messages):
    """Candidate # 1A"""
    msg = AppendEntriesMessage(2, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    follower.tick(250)
    assert messages[0].term == 3


def test_candidate_votes_for_self(follower, messages):
    """Candidate # 1B"""
    follower.transition_candidate()
    assert follower.voted_for == follower.node_id


def test_candidate_resets_timer_on_transition(follower, messages):
    """Candidate # 1C"""
    follower.state.timer = 50
    follower.transition_candidate()
    assert follower.state.timer == 0


def test_on_transition_to_candidate_send_request(follower, messages):
    """Candidate # 1D"""
    follower.tick(250)
    assert isinstance(messages[0], RequestVoteMessage)
    assert messages[0].candidate_id == "a"


def test_follower_accepts_append_log(follower):
    msg = AppendEntriesMessage(1, "b", 0, 0, [LogEntry(1, 1, "cmd")], 0)
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert isinstance(response, AppendEntriesResponse)
    assert response.term == 1
    assert response.success
    assert follower.state.node.last_log_index == 1


def test_follower_accepts_multiple_messages(follower):
    msg = AppendEntriesMessage(
        1, "b", 0, 0, [LogEntry(1, 1, "cmd"), LogEntry(2, 1, "cmd")], 0
    )
    response = follower.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert isinstance(response, AppendEntriesResponse)
    assert response.term == 1
    assert response.success
    assert follower.state.node.last_log_index == 2


def test_follower_accepts_multiple_append_requests(follower):
    msg = AppendEntriesMessage(1, "b", 0, 0, [LogEntry(1, 1, "cmd")], 0)
    msg2 = AppendEntriesMessage(1, "b", 1, 1, [LogEntry(2, 1, "cmd")], 0)
    msg3 = AppendEntriesMessage(1, "b", 2, 1, [LogEntry(3, 1, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    follower.process_message(Envelope(msg2, source_node="b", destination_node="a"))
    response = follower.process_message(
        Envelope(msg3, source_node="b", destination_node="a")
    )
    assert isinstance(response, AppendEntriesResponse)
    assert response.term == 1
    assert response.success
    assert follower.state.node.last_log_index == 3


def test_follower_corrects_log_entry(follower):
    msg = AppendEntriesMessage(1, "b", 0, 0, [LogEntry(1, 1, "cmd")], 0)
    msg2 = AppendEntriesMessage(1, "b", 1, 1, [LogEntry(7, 1, "cmd")], 0)
    msg3 = AppendEntriesMessage(1, "b", 1, 1, [LogEntry(2, 1, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    follower.process_message(Envelope(msg2, source_node="b", destination_node="a"))
    response = follower.process_message(
        Envelope(msg3, source_node="b", destination_node="a")
    )
    assert isinstance(response, AppendEntriesResponse)
    assert response.term == 1
    assert response.success
    assert follower.state.node.last_log_index == 2


def test_follower_append_fails_on_missing__prev_log_entry(follower):
    msg = AppendEntriesMessage(1, "b", 0, 0, [LogEntry(1, 1, "cmd")], 0)
    msg2 = AppendEntriesMessage(1, "b", 6, 1, [LogEntry(7, 1, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    response = follower.process_message(
        Envelope(msg2, source_node="b", destination_node="a")
    )
    assert isinstance(response, AppendEntriesResponse)
    assert response.term == 1
    assert response.success is False
    assert follower.state.node.last_log_index == 1


def test_follower_updates_term(follower):
    msg = AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0)
    msg2 = AppendEntriesMessage(2, "b", 6, 2, [LogEntry(7, 2, "cmd")], 0)
    msg3 = AppendEntriesMessage(1, "b", 2, 1, [LogEntry(3, 1, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    follower.process_message(Envelope(msg2, source_node="b", destination_node="a"))
    response = follower.process_message(
        Envelope(msg3, source_node="b", destination_node="a")
    )
    assert isinstance(response, AppendEntriesResponse)
    assert response.term == 2
    assert response.success is False
    assert follower.state.node.last_log_index == 7


def test_follower_updates_leader(follower):
    msg = AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0)
    follower.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert follower.state.node.cluster_membership.leader == "b"


def test_follower_grants_vote(follower):
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    response = follower.process_message(
        Envelope(
            RequestVoteMessage("a", 3, 6, 2), source_node="b", destination_node="a"
        )
    )
    assert response.vote_granted


def test_follower_doesnt_grant_vote_if_incomplete_log(follower):
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    response = follower.process_message(
        Envelope(
            RequestVoteMessage("a", 3, 2, 2), source_node="b", destination_node="a"
        )
    )
    assert response.vote_granted is False


def test_follower_doesnt_grant_vote_twice(follower):
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.process_message(
        Envelope(
            RequestVoteMessage("a", 3, 6, 2), source_node="b", destination_node="a"
        )
    )
    response = follower.process_message(
        Envelope(
            RequestVoteMessage("c", 3, 6, 2), source_node="c", destination_node="a"
        )
    )
    assert response.vote_granted is False


def test_follower_responsds_true_if_already_voted(follower):
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.process_message(
        Envelope(
            RequestVoteMessage("a", 3, 6, 2), source_node="b", destination_node="a"
        )
    )
    response = follower.process_message(
        Envelope(
            RequestVoteMessage("a", 3, 6, 2), source_node="b", destination_node="a"
        )
    )
    assert response.vote_granted


def test_follower_revotes_for_higher_term(follower):
    follower.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    follower.process_message(
        Envelope(
            RequestVoteMessage("a", 3, 6, 2), source_node="b", destination_node="a"
        )
    )
    response = follower.process_message(
        Envelope(
            RequestVoteMessage("c", 4, 6, 2), source_node="c", destination_node="a"
        )
    )
    assert response.vote_granted

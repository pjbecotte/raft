# pylint: disable=unused-argument
import pytest

from raft import AppendEntriesResponse, Envelope, LogEntry
from raft.messages import MembershipResponse, RemoveMemberMessage


def test_non_leader_responds_to_remove_membership_request(candidate):
    msg = candidate.process_message(
        Envelope(RemoveMemberMessage("d"), source_node="d", destination_node="a")
    )
    assert isinstance(msg, MembershipResponse)
    assert msg.status == "NOT_LEADER"


def test_follower_responds_to_remove_membership_request(follower):
    msg = follower.process_message(
        Envelope(RemoveMemberMessage("d"), source_node="d", destination_node="a")
    )
    assert isinstance(msg, MembershipResponse)
    assert msg.status == "NOT_LEADER"


def test_follower_leader_hint(follower):
    follower.cluster_membership.leader = "c"
    msg = follower.process_message(
        Envelope(RemoveMemberMessage("d"), source_node="d", destination_node="a")
    )
    assert isinstance(msg, MembershipResponse)
    assert msg.leader_hint == "c"


def test_responds_ok(leader, messages):
    leader.cluster_membership.members = {"a", "b", "c", "e"}
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.state.next_index["b"] = 56
    leader.state.next_index["c"] = 56
    leader.state.next_index["d"] = 56
    messages.empty()
    leader.process_message(
        Envelope(RemoveMemberMessage("e"), source_node="d", destination_node="a")
    )
    assert "cluster_membership" in messages.first_for_node("b").entries[-1].command
    assert "'e'" not in messages.first_for_node("b").entries[-1].command
    messages.empty()
    # quorum is now 2, so one ack should be enough
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="b", destination_node="a"
        )
    )
    assert messages.first_for_node("d").status == "OK"


def test_only_delete_one_at_a_time(leader, messages):
    leader.cluster_membership.members = {"a", "b", "c", "e", "f", "g"}
    leader.log.append(LogEntry(55, 5, ""))
    leader.current_term = 5
    leader.state.next_index["b"] = 56
    leader.state.next_index["c"] = 56
    leader.state.next_index["e"] = 56
    leader.state.next_index["f"] = 56
    leader.state.next_index["g"] = 56
    leader.process_message(
        Envelope(RemoveMemberMessage("e"), source_node="d", destination_node="a")
    )
    messages.empty()
    leader.process_message(
        Envelope(RemoveMemberMessage("f"), source_node="d", destination_node="a")
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="b", destination_node="a"
        )
    )
    # until the e removal commits, quorum should still be 3
    with pytest.raises(IndexError):
        messages.first_for_node("b")
    leader.process_message(
        Envelope(
            AppendEntriesResponse(5, 56, True), source_node="c", destination_node="a"
        )
    )
    assert "cluster_membership" in messages.first_for_node("b").entries[-1].command
    assert "'f'" not in messages.first_for_node("b").entries[-1].command

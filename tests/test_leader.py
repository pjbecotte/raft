# pylint: disable=unused-argument
from raft import (
    AppendEntriesMessage,
    AppendEntriesResponse,
    Envelope,
    Follower,
    Log,
    LogEntry,
    ProposeChangeMessage,
    RequestVoteMessage,
    RequestVoteResponse,
)
from raft.cluster_membership import ClusterMembership
from raft.node import Node


def test_ignores_tick_if_not_timeout(leader, messages):
    messages.empty()
    leader.tick(1)
    assert len(messages.calls) == 0


def test_reply_false_if_term_lt_current_term(leader, messages):
    """AppendEntries RPC # 1"""
    leader.current_term = 4
    msg = AppendEntriesMessage(2, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_reply_false_if_previndex_doesnt_match_prevterm(leader, messages):
    """AppendEntries RPC # 2"""
    leader.log.append(LogEntry(96, 5, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_reply_false_if_previndex_doesnt_exist(leader, messages):
    """AppendEntries RPC # 2"""
    leader.log.append(LogEntry(92, 2, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.success is False


def test_delete_conflict_if_higher_term(leader, messages):
    """AppendEntries RPC # 3"""
    starting_length = len(leader.log)
    leader.log.append(LogEntry(96, 5, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert len(leader.log) == starting_length


def test_append_new_entries(leader, messages):
    """AppendEntries RPC # 4"""
    leader.log.append(LogEntry(96, 4, ""))
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 0)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.log[-1].index == 97


def test_leadercommit_gt_commitindex_updates(leader, messages):
    """AppendEntries RPC # 5"""
    leader.log.append(LogEntry(96, 4, ""))
    leader.commit_index = 6
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 96)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.commit_index == 96


def test_commit_min_leadercommit_lastentry(leader, messages):
    """AppendEntries RPC # 5"""
    leader.log.append(LogEntry(96, 4, ""))
    leader.commit_index = 6
    msg = AppendEntriesMessage(5, "b", 96, 4, [LogEntry(97, 5, "cmd")], 99)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.commit_index == 97


def test_vote_reply_false_if_term_lt_current_term(leader, messages):
    """RequestVote RPC # 1"""
    leader.current_term = 4
    leader.log.append(LogEntry(96, 4, ""))
    msg = RequestVoteMessage("b", 3, 96, 4)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_successful_vote(leader, messages):
    """RequestVote RPC # 2"""
    leader.current_term = 4
    leader.voted_for = ""
    leader.log.append(LogEntry(96, 4, ""))
    msg = RequestVoteMessage("b", 5, 96, 4)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted


def test_vote_fails_if_voted(leader, messages):
    """RequestVote RPC # 2"""
    leader.current_term = 5
    leader.voted_for = "c"
    leader.log.append(LogEntry(96, 4, ""))
    msg = RequestVoteMessage("b", 5, 96, 4)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_vote_fails_if_candidate_not_up_to_date(leader, messages):
    """RequestVote RPC # 2"""
    leader.current_term = 4
    leader.voted_for = ""
    leader.log.append(LogEntry(96, 4, ""))
    msg = RequestVoteMessage("b", 5, 95, 4)
    response = leader.process_message(
        Envelope(msg, source_node="b", destination_node="a")
    )
    assert response.vote_granted is False


def test_update_term_convert_to_follower_append_entries(leader, messages):
    """All Servers # 2"""
    leader.current_term = 4
    msg = AppendEntriesMessage(5, "b", 0, 0, [LogEntry(2, 2, "cmd")], 0)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.current_term == 5
    assert isinstance(leader.state, Follower)


def test_update_term_convert_to_follower_ae_response(leader, messages):
    """All Servers # 2"""
    leader.current_term = 4
    msg = AppendEntriesResponse(5, 3, True)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.current_term == 5
    assert isinstance(leader.state, Follower)


def test_update_term_convert_to_follower_vote(leader, messages):
    """All Servers # 2"""
    leader.current_term = 4
    msg = RequestVoteMessage("b", 5, 3, 3)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.current_term == 5
    assert isinstance(leader.state, Follower)


def test_update_term_convert_to_follower_vote_response(leader, messages):
    """All Servers # 2"""
    leader.current_term = 4
    msg = RequestVoteResponse(5, True)
    leader.process_message(Envelope(msg, source_node="b", destination_node="a"))
    assert leader.current_term == 5
    assert isinstance(leader.state, Follower)


def test_leader_initializes_log_index(leader, messages):
    """Leader # 1"""
    msg = messages[-3]
    assert isinstance(msg, AppendEntriesMessage)
    assert msg.term == 3
    assert msg.prev_log_index == 6
    assert msg.prev_log_term == 2
    assert msg.entries == []


def test_sends_heartbeats(leader, messages):
    """Leader # 1B"""
    messages.empty()
    leader.tick(25)
    assert messages[-1]


def test_leader_commits_no_op(leader, messages):
    leader.process_message(
        Envelope(
            AppendEntriesResponse(3, 6, True), source_node="b", destination_node="a"
        )
    )
    messages.empty()
    leader.tick(25)
    assert messages[0].entries[0].index == 7
    assert messages[0].commit_index == 6


def test_leader_starts_at_nextid(leader, messages):
    """Leader # 3A"""
    leader.log = Log()
    leader.log.append(LogEntry(5, 2, ""))
    leader.log.append(LogEntry(6, 2, ""))
    leader.log.append(LogEntry(7, 2, ""))
    leader.state.next_index["c"] = 6
    messages.empty()
    leader.tick(25)
    assert messages.first_for_node("c").entries[0].index == 6


def test_leader_updates_index_on_success(leader, messages):
    """Leader # 3B"""
    leader.log = Log()
    leader.log.append(LogEntry(5, 2, ""))
    leader.log.append(LogEntry(6, 2, ""))
    leader.log.append(LogEntry(7, 2, ""))
    leader.state.match_index["c"] = 3
    leader.state.next_index["c"] = 7
    leader.process_message(
        Envelope(
            AppendEntriesResponse(2, 7, True), source_node="c", destination_node="a"
        )
    )
    assert leader.state.next_index["c"] == 8
    assert leader.state.match_index["c"] == 7


def test_leader_retries_append(leader, messages):
    """Leader # 3C"""
    leader.log = Log()
    leader.log.append(LogEntry(5, 2, ""))
    leader.log.append(LogEntry(6, 2, ""))
    leader.log.append(LogEntry(7, 2, ""))
    leader.state.next_index["c"] = 7
    messages.empty()
    leader.process_message(
        Envelope(
            AppendEntriesResponse(2, 5, False), source_node="c", destination_node="a"
        )
    )
    assert leader.state.next_index["c"] == 6
    assert messages.first_for_node("c").entries[0].index == 6


def test_leader_accepts_log_entry(messages, leader):
    """Leader # 2A"""
    leader.process_message(
        Envelope(
            AppendEntriesResponse(3, 7, True), source_node="b", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(3, 7, True), source_node="c", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command"), source_node="f", destination_node="a"
        )
    )
    msg = messages[-1]
    assert isinstance(msg, AppendEntriesMessage)
    assert msg.term == 3
    assert msg.prev_log_index == 7
    assert len(msg.entries) == 1
    idx = msg.entries[0].index
    leader.process_message(
        Envelope(
            AppendEntriesResponse(3, idx, True), source_node="b", destination_node="a"
        )
    )
    messages.empty()
    leader.tick(25)
    msg = messages.first_for_node("b")
    assert isinstance(msg, AppendEntriesMessage)
    assert msg.term == 3
    assert msg.prev_log_index == idx
    assert len(msg.entries) == 0
    assert msg.commit_index == idx


def test_commits_after_success(messages, leader):
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command"), source_node="f", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command 2"),
            source_node="f",
            destination_node="a",
        )
    )
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command 3"),
            source_node="f",
            destination_node="a",
        )
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(3, 9, True), source_node="b", destination_node="a"
        )
    )
    messages.empty()
    leader.tick(25)
    msg = messages.first_for_node("b")
    # There is an uncommited entry that should be sent out on heartbeat
    assert len(msg.entries) == 1
    assert msg.commit_index == 9


def test_steps_down_if_higher_term(messages, leader):
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command"), source_node="f", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            AppendEntriesResponse(4, 9, False), source_node="b", destination_node="a"
        )
    )
    assert isinstance(leader.state, Follower)


def test_steps_down_if_higher_term_received(messages, leader):
    leader.process_message(
        Envelope(
            AppendEntriesMessage(4, "b", 8, 4, [LogEntry(9, 4, "cmd")], 0),
            source_node="b",
            destination_node="a",
        )
    )
    assert isinstance(leader.state, Follower)


def test_retries_on_failure(messages, leader):
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command"), source_node="f", destination_node="a"
        )
    )
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command 2"),
            source_node="f",
            destination_node="a",
        )
    )
    leader.process_message(
        Envelope(
            ProposeChangeMessage("some command 3"),
            source_node="f",
            destination_node="a",
        )
    )
    messages.empty()
    leader.process_message(
        Envelope(
            AppendEntriesResponse(3, 0, False), source_node="b", destination_node="a"
        )
    )
    assert messages[-1].entries[0].index == 6
    assert len(messages[-1].entries) == 5


def test_needs_majority_for_commit(messages):
    """Leader # 4"""
    node = Node("a", cluster_membership=ClusterMembership({"a", "b", "c", "d", "e"}))
    node.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 6),
            source_node="b",
            destination_node="a",
        )
    )
    node.tick(250)
    node.process_message(
        Envelope(RequestVoteResponse(3, True), source_node="b", destination_node="a")
    )
    node.process_message(
        Envelope(RequestVoteResponse(3, True), source_node="c", destination_node="a")
    )
    node.process_message(
        Envelope(
            ProposeChangeMessage("some command"), source_node="f", destination_node="a"
        )
    )
    node.process_message(
        Envelope(
            ProposeChangeMessage("some command 2"),
            source_node="f",
            destination_node="a",
        )
    )
    node.process_message(
        Envelope(
            ProposeChangeMessage("some command 3"),
            source_node="f",
            destination_node="a",
        )
    )
    node.process_message(
        Envelope(
            AppendEntriesResponse(3, 9, True), source_node="b", destination_node="a"
        )
    )
    messages.empty()
    node.tick(25)
    msg = messages.first_for_node("b")
    # There is an uncommited entry that should be sent out on heartbeat
    assert len(msg.entries) == 1
    assert msg.commit_index == 6


def test_does_not_commit_entry_from_previous_term(messages):
    node = Node("a", cluster_membership=ClusterMembership({"a", "b", "c"}))
    node.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 6),
            source_node="b",
            destination_node="a",
        )
    )
    node.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 6, 2, [LogEntry(7, 2, "cmd")], 6),
            source_node="b",
            destination_node="a",
        )
    )
    node.tick(250)
    node.process_message(
        Envelope(RequestVoteResponse(3, True), source_node="b", destination_node="a")
    )
    node.process_message(
        Envelope(
            AppendEntriesResponse(3, 7, True), source_node="b", destination_node="a"
        )
    )
    messages.empty()
    node.tick(25)
    assert messages[0].commit_index == 6


def test_steps_down_if_election_timeout_expires_with_no_responses(leader, messages):
    leader.tick(250)
    assert isinstance(leader.state, Follower)

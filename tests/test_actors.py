from raft.actors.actor import Actor
from raft.messages import (
    ActorStartedMessage,
    ActorTerminatedMessage,
    AddMemberMessage,
    Envelope,
)


def test_run_an_actor(supe):
    actor = Actor("my actor")
    supe.start_actor(actor)
    assert isinstance(supe.test_messages.get(True, 1).msg, ActorStartedMessage)
    supe.kill_actor("my actor")
    assert isinstance(supe.test_messages.get(True, 1).msg, ActorTerminatedMessage)


def test_echo_actor(supe):
    class EchoActor(Actor):
        async def handle_message(self, env: Envelope):
            self.send_message(Envelope(AddMemberMessage("")))

    actor = EchoActor("my actor")
    supe.start_actor(actor)
    assert isinstance(supe.test_messages.get(True, 1).msg, ActorStartedMessage)
    supe.send_to_actor(Envelope(AddMemberMessage(""), destination_node="my actor"))
    assert isinstance(supe.test_messages.get(True, 1).msg, AddMemberMessage)

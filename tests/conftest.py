# pylint: disable=unused-argument
from multiprocessing import Queue
from random import randint
from unittest.mock import _Call, patch

import pytest

from raft import AppendEntriesMessage, Envelope, LogEntry, Node, RequestVoteResponse
from raft.actors.supervisor import Supervisor
from raft.cluster_membership import ClusterMembership
from raft.messages import SupervisorAddress


def extract_env(call: _Call):
    """the _Call object added .args as a property in 3.8, but 3.7 didnt have it
    and would actually return a fake result if you tried it, so we have to
    extract the args ourselves just in case
    """
    for item in call:
        if isinstance(item, tuple):
            return item[0]
    raise IndexError("This isnt a _Call object?")


class MessageProxy:
    def __init__(self, mock):
        self.calls = mock.mock_calls

    def __getitem__(self, item):
        return extract_env(self.calls[item]).msg

    def first_for_node(self, node_id):
        for call in self.calls:
            if extract_env(call).destination_node == node_id:
                return extract_env(call).msg
        raise IndexError("No msg for that node")

    def empty(self):
        self.calls.clear()


@pytest.fixture(name="messages")
def _messages():
    with patch("raft.actors.actor.Actor.send_message") as mock:
        yield MessageProxy(mock)


class MockSupervisor(Supervisor):
    def __init__(self):
        # want to pick a random port so that concurrent runs dont fail
        port_num = randint(20000, 35000)
        super().__init__(SupervisorAddress(port=port_num))
        self.test_messages = Queue()

    def handle_supervisor_message(self, env: Envelope):
        self.test_messages.put(env)


@pytest.fixture(name="supe")
def _supe():
    supe = MockSupervisor()
    supe.test_messages.get(True, 1)
    yield supe
    supe.kill_all_actors()


@pytest.fixture(name="supe2")
def _supe_2():
    supe = MockSupervisor()
    supe.test_messages.get(True, 1)
    yield supe
    supe.kill_all_actors()


@pytest.fixture(name="follower")
def _follower():
    return Node("a", cluster_membership=ClusterMembership({"a", "b", "c"}))


@pytest.fixture(name="candidate")
def _candidate(follower, messages):
    follower.tick(250)
    return follower


@pytest.fixture(name="leader")
def _leader(messages):
    node = Node("a", cluster_membership=ClusterMembership({"a", "b", "c"}))
    node.process_message(
        Envelope(
            AppendEntriesMessage(2, "b", 0, 0, [LogEntry(6, 2, "cmd")], 6),
            source_node="b",
            destination_node="a",
        )
    )
    node.tick(250)
    node.process_message(
        Envelope(RequestVoteResponse(3, True), source_node="b", destination_node="a")
    )
    return node

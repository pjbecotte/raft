FROM ubuntu:latest

WORKDIR /app

RUN apt-get update && apt-get install -y software-properties-common gcc && rm -rf /var/lib/apt/lists/*

RUN apt-get update && apt-add-repository ppa:deadsnakes/ppa && \
  apt-get update && \
  apt-get install -y \
  	git \
    python3.6-dev \
    python3.7-dev \
    python3.8-dev \
    python3.8-venv \
    wget \
 && rm -rf /var/lib/apt/lists/*

RUN wget -nv https://bootstrap.pypa.io/get-pip.py -O /tmp/get-pip.py \
	&& python3.8 /tmp/get-pip.py \
	&& rm /tmp/get-pip.py

RUN ln /usr/bin/python3.8 /usr/bin/python
RUN pip install pipx virtualenv
ENV PATH=/root/.local/bin:$PATH
RUN pipx install poetry==1.0.0b4
RUN pipx install rye==0.4.3
ENV POETRY_VIRTUALENVS_CREATE=false
ENV PY_IGNORE_IMPORTMISMATCH=1
COPY pyproject.toml poetry.lock rye.yaml ./
RUN rye -c .docker build-envs

from dataclasses import dataclass, field
from typing import Dict, Set


@dataclass
class NewServer:
    requester: str
    next_index: int = 0
    rounds: int = 0
    last_response: int = 0
    success: bool = False


@dataclass
class ClusterMembership:
    members: Set[str] = field(default_factory=set)
    leader: str = ""
    new_servers: Dict[str, NewServer] = field(default_factory=dict)
    servers_to_remove: Dict[str, str] = field(default_factory=dict)

    def add_server(self, node_name, requester):
        self.new_servers[node_name] = NewServer(requester)

import asyncio
from multiprocessing import Queue
from typing import Optional

from raft.actors.mailbox import SupervisorMailbox
from raft.messages import (
    SUPERVISOR,
    ActorStartedMessage,
    ActorTerminatedMessage,
    Envelope,
    ShutdownActorMessage,
)


class Actor:
    def __init__(self, name: str):
        self.name: str = name
        self.outbox: asyncio.Queue
        self.supervisor: Optional[SupervisorMailbox] = None
        self.running: bool = False

    def run(self, inbox: Queue, outbox: Queue):
        self.supervisor = SupervisorMailbox(inbox, outbox)
        self.run_loop()

    def run_loop(self):
        asyncio.run(self.master_loop())

    async def background_processes(self):
        pass

    async def master_loop(self):
        self.outbox = asyncio.Queue()
        self.running = True
        await self.notify_supervisor_started()
        try:
            await asyncio.gather(
                self.receive_from_supervisor(),
                self.watch_outbox(),
                self.background_processes(),
            )
        except asyncio.CancelledError:
            pass

    async def notify_supervisor_started(self):
        self.send_message(Envelope(ActorStartedMessage(), destination_node=SUPERVISOR))

    async def shutdown(self):
        self.send_message(
            Envelope(ActorTerminatedMessage(), destination_node=SUPERVISOR)
        )
        self.running = False

    async def receive_from_supervisor(self):
        while self.running:
            env = await self.supervisor.recv()
            if env and isinstance(env.msg, ShutdownActorMessage):
                await self.shutdown()
            elif env:
                asyncio.create_task(self.handle_message(env))

    def send_message(self, env: Envelope):
        self.outbox.put_nowait(env)

    async def watch_outbox(self):
        while self.running or not self.outbox.empty():
            try:
                env = await asyncio.wait_for(self.outbox.get(), timeout=0.1)
                await self.supervisor.send(env)
            except asyncio.TimeoutError:
                pass

    async def handle_message(self, env: Envelope):
        pass

from _queue import Empty
from dataclasses import dataclass
from multiprocessing import Process, Queue
from threading import Thread
from typing import Dict

from raft.actors.actor import Actor
from raft.actors.network_actor import NetworkActor
from raft.messages import Envelope, ShutdownActorMessage, SupervisorAddress

NETWORK_ACTOR = "NetworkActor"


@dataclass
class ActorRecord:
    actor: Actor
    proc: Process
    queue: Queue


class Supervisor:
    def __init__(self, address: SupervisorAddress):
        self.address = address
        self.actors: Dict[str, ActorRecord] = {}
        self.queue: Queue = Queue()
        self.running = False
        self.thread = Thread(target=self.listen_to_queue)
        self.thread.start()

    def start_actor(self, actor: Actor):
        actor_inbox: Queue = Queue()
        proc = Process(target=actor.run, args=(actor_inbox, self.queue))
        record = ActorRecord(actor, proc, actor_inbox)
        self.actors[actor.name] = record
        proc.start()

    def kill_actor(self, actor_name: str):
        env = Envelope(ShutdownActorMessage(), destination_node=actor_name)
        self.send_to_actor(env)
        self.actors[actor_name].proc.join()
        del self.actors[actor_name]

    def kill_all_actors(self):
        for actor_name in self.actors:
            self.send_to_actor(
                Envelope(ShutdownActorMessage(), destination_node=actor_name)
            )
        for actor in self.actors.values():
            actor.proc.join()
        self.actors = {}
        self.running = False
        self.thread.join()

    def send_to_actor(self, env: Envelope):
        self.actors[env.destination_node].queue.put(env)

    def send_network(self, env: Envelope):
        self.actors[NETWORK_ACTOR].queue.put(env)

    def handle_supervisor_message(self, env: Envelope):
        pass

    def listen_to_queue(self):
        self.start_actor(NetworkActor(NETWORK_ACTOR, self.address))
        self.running = True
        while self.running:
            try:
                env = self.queue.get(timeout=0.1)
                if not isinstance(env, Envelope):
                    continue
                if env.destination_node in self.actors:
                    self.send_to_actor(env)
                elif env.destination_addr.port:
                    self.send_network(env)
                else:
                    self.handle_supervisor_message(env)
            except Empty:
                pass

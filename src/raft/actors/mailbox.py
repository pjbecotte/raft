import asyncio
from multiprocessing import Queue
from queue import Empty
from typing import Optional

from raft.messages import Envelope


class SupervisorMailbox:
    def __init__(self, inbox: Queue, outbox: Queue):
        self.inbox = inbox
        self.outbox = outbox

    async def recv(self) -> Optional[Envelope]:
        loop = asyncio.get_running_loop()
        try:
            return await loop.run_in_executor(None, self.inbox.get, True, 0.1)
        except Empty:
            return None

    async def send(self, env: Envelope):
        loop = asyncio.get_running_loop()
        return await loop.run_in_executor(None, self.outbox.put, env, True, 0.1)

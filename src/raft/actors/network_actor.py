import asyncio
import pickle
from asyncio import StreamReader, StreamWriter

from raft import Envelope
from raft.actors.actor import Actor
from raft.messages import SupervisorAddress


class NetworkActor(Actor):
    def __init__(self, name, address: SupervisorAddress):
        super().__init__(name)
        self.address = address
        self.tcp_server = None

    async def handle_message(self, env: Envelope):
        if env.destination_node != self.name:
            await self.ship_message(env)

    async def background_processes(self):
        await self.listen_for_messages()

    async def listen_for_messages(self):
        self.tcp_server = await asyncio.start_server(
            self.handle_connection, self.address.address, self.address.port
        )
        async with self.tcp_server:
            await self.tcp_server.serve_forever()

    async def shutdown(self):
        self.tcp_server.close()

    async def handle_connection(self, reader: StreamReader, _: StreamWriter):
        data = await reader.read()
        env = pickle.loads(data)
        self.send_message(env)

    async def ship_message(self, envelope: Envelope):
        dest = envelope.destination_addr
        envelope.source_addr = self.address
        _, writer = await asyncio.open_connection(dest.address, dest.port)
        package = pickle.dumps(envelope)
        writer.write(package)
        writer.close()
        await writer.wait_closed()

from collections import OrderedDict
from dataclasses import dataclass
from itertools import islice
from typing import List, Optional, Union


@dataclass
class LogEntry:
    index: int
    term: int
    command: str


class Log:
    """
    A data structure to represent the state-machine log for a RAFT Node.
    Since each object has an index number built into it, but there is no
    guarantee that there will be a record for every entry, this proxies
    index lookups to look at the index field on the log records.
    """

    def __init__(self):
        self.entries = OrderedDict()

    @property
    def last_index(self):
        if not self.entries:
            return 0
        return next(reversed(self.entries))

    def previous(self, idx: int) -> Optional[LogEntry]:
        return next(self.__handle_slice(slice(idx - 1, 0, -1)), None)

    def append(self, entry: LogEntry):
        if entry.index <= self.last_index:
            raise KeyError()
        self.entries[entry.index] = entry

    def pop(self):
        if self.entries:
            self.entries.popitem(last=True)

    def extend(self, entries):
        for entry in entries:
            self.append(entry)

    def __getitem__(self, item) -> Union[LogEntry, List[LogEntry], None]:
        if isinstance(item, slice):
            return list(self.__handle_slice(item))
        if item < 0:
            return next(
                islice(reversed(self.entries.values()), ((item - 1) * -1)), None
            )
        return self.entries[item]

    def __handle_slice(self, slice_object: slice):
        step = slice_object.step or 1
        next_idx = slice_object.start or (self.last_index if step < 0 else 1)
        last_idx = slice_object.stop or (1 if step < 0 else self.last_index + 1)

        def continue_iteration():
            if step < 0:
                return next_idx > last_idx
            return next_idx < last_idx

        while continue_iteration():
            if next_idx in self.entries:
                yield self.entries[next_idx]
            next_idx += step

    def __bool__(self):
        return bool(self.entries)

    def __len__(self):
        return len(self.entries)

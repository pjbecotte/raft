from dataclasses import dataclass, field
from time import time
from typing import Dict, List

from raft.log import LogEntry

SUPERVISOR = "SUPERVISOR"


@dataclass
class SupervisorAddress:
    address: str = "localhost"
    port: int = 0
    last_heartbeat: float = field(default_factory=time)
    communication: str = "tcp"

    def __eq__(self, other):
        return self.address == other.address and self.port == other.port


class Message:
    term: int


class Envelope:
    def __init__(
        self,
        msg: Message,
        *,
        source_node: str = "",
        destination_node: str = "",
        source_addr: SupervisorAddress = None,
        destination_addr: SupervisorAddress = None,
        known_supervisors: Dict[str, SupervisorAddress] = None,
    ):
        self.known_supervisors: Dict[str, SupervisorAddress] = known_supervisors or {}
        self.source_addr = source_addr or SupervisorAddress()
        self.destination_addr = destination_addr or SupervisorAddress()
        self.msg = msg
        self.source_node = source_node
        self.destination_node = destination_node


@dataclass
class RequestVoteMessage(Message):
    candidate_id: str
    term: int
    last_log_index: int
    last_log_term: int


@dataclass
class RequestVoteResponse(Message):
    term: int
    vote_granted: bool


@dataclass
class AppendEntriesMessage(Message):
    term: int
    leader_id: str
    prev_log_index: int
    prev_log_term: int
    entries: List[LogEntry]
    commit_index: int


@dataclass
class AppendEntriesResponse(Message):
    term: int
    index: int
    success: bool


class ClientMessage(Message):
    pass


@dataclass
class ProposeChangeMessage(ClientMessage):
    command: str


@dataclass
class AddMemberMessage(ClientMessage):
    new_server: str


@dataclass
class RemoveMemberMessage(ClientMessage):
    old_server: str


@dataclass
class MembershipResponse(ClientMessage):
    status: str
    leader_hint: str


class ActorStartedMessage(Message):
    pass


class ActorTerminatedMessage(Message):
    pass


class ShutdownActorMessage(Message):
    pass

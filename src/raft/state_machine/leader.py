from dataclasses import dataclass
from math import floor
from typing import Dict

from raft.log import LogEntry
from raft.messages import (
    AddMemberMessage,
    AppendEntriesMessage,
    AppendEntriesResponse,
    ClientMessage,
    MembershipResponse,
    Message,
    ProposeChangeMessage,
    RemoveMemberMessage,
    RequestVoteMessage,
    RequestVoteResponse,
)
from raft.state_machine.state import State


@dataclass
class ClientRequest:
    requester: str
    msg: Message


class Leader(State):
    def __init__(self, node: "Node"):  # type: ignore
        super().__init__(node)
        self.timeout = 250
        self.heartbeat_timer = 0
        self.heartbeat_timeout = 25
        self.next_index = {
            node_id: self.last_log_index + 1
            for node_id in self.cluster_membership.members
        }
        self.match_index = {node_id: 0 for node_id in self.cluster_membership.members}
        self.client_requests: Dict[int, ClientRequest] = {}
        self.initialize()

    def initialize(self):
        """$3.4
        Initialize the leader by sending an empty heartbeat to each follower,
        then add a no-op to the log
        """
        self._heartbeat()
        self.add_change("")

    def request_vote(self, msg: RequestVoteMessage) -> RequestVoteResponse:
        return RequestVoteResponse(self.current_term, False)

    def propose_change(self, msg: ProposeChangeMessage):
        self.add_change(msg.command)

    def add_change(self, cmd: str, client=None, message=None):
        log_entry = LogEntry(self.last_log_index + 1, self.current_term, cmd)
        self.log.append(log_entry)
        if client and message:
            self.client_requests[log_entry.index] = ClientRequest(client, message)
        self._heartbeat()

    def send_heartbeat(self, node_id):
        next_index = self.next_index[node_id]
        self.send_append(node_id, next_index)

    def send_catchup(self, node_id):
        self.cluster_membership.new_servers[node_id].rounds += 1
        if self.cluster_membership.new_servers[node_id].rounds > 10:
            self.send_message(
                MembershipResponse("TIMEOUT", self.node_id),
                {self.cluster_membership.new_servers[node_id].requester},
            )

            del self.cluster_membership.new_servers[node_id]
            return
        next_index = (
            self.cluster_membership.new_servers[node_id].next_index
            or self.last_log_index + 1
        )
        self.send_append(node_id, next_index)

    def send_append(self, node_id, next_index):
        entries = self.log[next_index:]
        prev_entry = self.log.previous(next_index)
        prev_index, prev_term = 0, 0
        if prev_entry:
            prev_index, prev_term = prev_entry.index, prev_entry.term
        self.send_message(
            AppendEntriesMessage(
                self.current_term,
                self.node_id,
                prev_index,
                prev_term,
                entries,
                self.commit_index,
            ),
            {node_id},
        )

    def append_entries(self, msg: AppendEntriesMessage) -> AppendEntriesResponse:
        return AppendEntriesResponse(self.current_term, self.last_log_index, False)

    def append_entries_response(self, msg: AppendEntriesResponse, source_node: str):
        """$3.5
        The response from a node to an append entries rpc. If its successful,
        we can update the replicated index for that node. if not, look at the index
        that node claims to be its last index and try to update it from there. If
        a node reports a higher term, step down.
        """
        if msg.success and source_node in self.cluster_membership.members:
            self.match_index[source_node] = msg.index
            self.next_index[source_node] = msg.index + 1
            self._update_commit_index()
        elif (
            msg.success
            and source_node in self.cluster_membership.new_servers
            and self._cluster_membership_committed()
        ):
            self.next_index[source_node] = msg.index + 1
            self.match_index[source_node] = msg.index
            self._add_member(source_node)
        elif msg.success and source_node in self.cluster_membership.new_servers:
            self.cluster_membership.new_servers[source_node].success = True
            self.next_index[source_node] = msg.index + 1
            self.match_index[source_node] = msg.index
        elif source_node in self.cluster_membership.new_servers:
            self.cluster_membership.new_servers[source_node].next_index = msg.index + 1
            self.send_catchup(source_node)
        else:
            self.next_index[source_node] = msg.index + 1
            self.send_heartbeat(source_node)

    def _cluster_membership_committed(self):
        for msg in self.log[::-1]:
            if "cluster_membership" in msg.command:
                return self.commit_index >= msg.index
        return True

    def _add_member(self, member):
        self.cluster_membership.members.add(member)
        self.add_change(
            f"cluster_membership = {self.cluster_membership.members}",
            self.cluster_membership.new_servers[member].requester,
            MembershipResponse("OK", self.node_id),
        )
        del self.cluster_membership.new_servers[member]

    def _remove_member(self, member, client):
        self.cluster_membership.members.remove(member)
        self.add_change(
            f"cluster_membership = {self.cluster_membership.members}",
            client,
            MembershipResponse("OK", self.node_id),
        )

    def _check_uncommitted_membership(self):
        if self.cluster_membership.new_servers:
            name, node = next(iter(self.cluster_membership.new_servers.items()))
            if node.success:
                self._add_member(name)
        elif self.cluster_membership.servers_to_remove:
            self._remove_member(
                *next(iter(self.cluster_membership.servers_to_remove.items()))
            )

    def _update_commit_index(self):
        """$3.5, $3.6
        A log entry is committed when it has been replicated to a quorum of
        nodes. Advance the tracker to the highest index that is present on a
        majority of known members. Finally, we only allow a leader to directly
        commit entries from its own term so that it must have received at least
        one round of successful RPCs before committing
        """
        quorum = len(self.cluster_membership.members) / 2
        last_committed = self.commit_index
        self.match_index[self.node_id] = self.last_log_index
        committed = sorted(self.match_index.values(), reverse=True)[floor(quorum)]
        if committed > last_committed and self.log[committed].term == self.current_term:
            self.commit_index = max(committed, self.commit_index)
            self._check_uncommitted_membership()
            self._heartbeat()
            for idx in range(last_committed + 1, committed + 1):
                if idx in self.client_requests:
                    self.send_message(
                        self.client_requests[idx].msg,
                        {self.client_requests[idx].requester},
                    )

    def _heartbeat(self):
        for node in self.cluster_membership.members:
            if node != self.node_id:
                self.send_heartbeat(node)

    def tick(self, ticks: int):
        self.timer += ticks
        self.heartbeat_timer += ticks
        self._update_commit_index()
        if self.timer >= self.timeout:
            self.step_down()
        elif self.heartbeat_timer >= self.heartbeat_timeout:
            self._heartbeat()

    def step_down(self):
        self.node.transition_follower()

    def process_client_messsage(self, msg: ClientMessage, source_node: str):
        if isinstance(msg, ProposeChangeMessage):
            return self.propose_change(msg)

        if isinstance(msg, AddMemberMessage):
            node = msg.new_server
            self.cluster_membership.add_server(node, source_node)
            self.send_catchup(node)

        if isinstance(msg, RemoveMemberMessage):
            node = msg.old_server
            if self._cluster_membership_committed():
                self._remove_member(node, source_node)
            else:
                self.cluster_membership.servers_to_remove[node] = source_node

        return None

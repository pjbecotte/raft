from random import randint

from raft.messages import AppendEntriesMessage, AppendEntriesResponse
from raft.state_machine.state import State


class Follower(State):
    def __init__(self, node: "Node"):  # type: ignore
        super().__init__(node)
        self.timeout = randint(100, 250)

    def expire(self):
        self.node.transition_candidate()

    def append_entries(self, msg: AppendEntriesMessage) -> AppendEntriesResponse:
        if msg.term < self.current_term:
            return AppendEntriesResponse(self.current_term, self.last_log_index, False)
        self.cluster_membership.leader = msg.leader_id
        self.timer = 0
        while self.last_log_index > msg.prev_log_index:
            self.log.pop()
        if (
            self.last_log_index == msg.prev_log_index
            and self.last_log_term != msg.prev_log_term
        ):
            self.log.pop()
        if (
            self.last_log_index != msg.prev_log_index
            or self.last_log_term != msg.prev_log_term
        ):
            return AppendEntriesResponse(msg.term, self.last_log_index, False)
        self.log.extend(msg.entries)
        if msg.commit_index > self.commit_index:
            self.commit_index = min(self.last_log_index, msg.commit_index)
        return AppendEntriesResponse(msg.term, self.last_log_index, True)

from random import randint

from raft.messages import (
    AppendEntriesMessage,
    AppendEntriesResponse,
    RequestVoteMessage,
    RequestVoteResponse,
)
from raft.state_machine.state import State


class Candidate(State):
    votes_received: set

    def __init__(self, node: "Node"):  # type: ignore
        super().__init__(node)
        self.timeout = randint(100, 250)
        self.start_election()

    def start_election(self):
        self.node.increment_term()
        self.node.voted_for = self.node.node_id
        self.votes_received = {self.node.node_id}
        self.send_message(
            RequestVoteMessage(
                self.node_id,
                self.current_term,
                self.last_log_index,
                self.last_log_term,
            )
        )

    def request_vote_response(self, msg: RequestVoteResponse, source_node: str):
        if msg.vote_granted and msg.term == self.current_term:
            self.votes_received.add(source_node)
        if len(self.votes_received) > len(self.cluster_membership.members) / 2:
            self.node.transition_leader()

    def append_entries(self, msg: AppendEntriesMessage) -> AppendEntriesResponse:
        if msg.term < self.current_term:
            return AppendEntriesResponse(self.current_term, self.last_log_index, False)
        self.step_down()
        return self.node.state.append_entries(msg)

    def expire(self):
        self.start_election()

    def step_down(self):
        self.node.transition_follower()

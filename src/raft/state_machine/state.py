from typing import Optional

from raft.cluster_membership import ClusterMembership
from raft.log import Log
from raft.messages import (
    AddMemberMessage,
    AppendEntriesMessage,
    AppendEntriesResponse,
    ClientMessage,
    Envelope,
    MembershipResponse,
    Message,
    ProposeChangeMessage,
    RemoveMemberMessage,
    RequestVoteMessage,
    RequestVoteResponse,
)


class State:
    timeout: int

    def __init__(self, node: "Node"):  # type: ignore
        self.timer = 0
        self.node = node

    @property
    def node_id(self) -> str:
        return self.node.node_id

    @property
    def cluster_membership(self) -> ClusterMembership:
        return self.node.cluster_membership

    @property
    def current_term(self) -> int:
        return self.node.current_term

    @property
    def commit_index(self) -> int:
        return self.node.commit_index

    @commit_index.setter
    def commit_index(self, val):
        self.node.commit_index = val

    @property
    def last_log_index(self) -> int:
        return self.node.last_log_index

    @property
    def last_log_term(self) -> int:
        return self.node.last_log_term

    @property
    def log(self) -> Log:
        return self.node.log

    def process_message(self, msg: Message, source_node: str) -> Optional[Message]:
        if isinstance(msg, AppendEntriesMessage):
            return self.append_entries(msg)

        if isinstance(msg, AppendEntriesResponse):
            return self.append_entries_response(msg, source_node)

        if isinstance(msg, RequestVoteMessage):
            return self.request_vote(msg)

        assert isinstance(msg, RequestVoteResponse)
        return self.request_vote_response(msg, source_node)

    def process_client_messsage(
        self, msg: ClientMessage, source_node: str  # pylint: disable=unused-argument
    ):
        if isinstance(msg, ProposeChangeMessage):
            return self.propose_change(msg)

        assert isinstance(msg, (AddMemberMessage, RemoveMemberMessage))
        return MembershipResponse(
            status="NOT_LEADER", leader_hint=self.cluster_membership.leader
        )

    def expire(self):
        raise NotImplementedError()

    def tick(self, ticks: int):
        self.timer += ticks
        if self.timer >= self.timeout:
            self.expire()

    def append_entries(self, msg: AppendEntriesMessage) -> AppendEntriesResponse:
        raise NotImplementedError()

    def append_entries_response(self, msg: AppendEntriesResponse, source_node: str):
        pass

    def request_vote(self, msg: RequestVoteMessage) -> RequestVoteResponse:
        vote = (
            msg.term == self.current_term
            and msg.last_log_index >= self.last_log_index
            and self.node.voted_for in ("", msg.candidate_id)
        )
        if vote:
            self.node.voted_for = msg.candidate_id
            self.timer = 0
        return RequestVoteResponse(self.current_term, vote)

    def request_vote_response(self, msg: RequestVoteResponse, source_node: str):
        pass

    def propose_change(self, msg: ProposeChangeMessage):
        raise NotImplementedError()

    def send_message(self, msg: Message, destination: Optional[set] = None):
        destination = destination or self.cluster_membership.members
        for dest in destination:
            if dest == self.node_id:
                continue
            env = Envelope(msg, source_node=self.node_id, destination_node=dest)
            self.node.send_message(env)

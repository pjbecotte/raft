from typing import Optional

from raft.actors.actor import Actor
from raft.cluster_membership import ClusterMembership
from raft.log import Log
from raft.messages import ClientMessage, Envelope
from raft.state_machine.candidate import Candidate
from raft.state_machine.follower import Follower
from raft.state_machine.leader import Leader
from raft.state_machine.state import State


class Node(Actor):
    def __init__(
        self, node_id: str, cluster_membership: Optional[ClusterMembership] = None,
    ):
        super().__init__(node_id)
        self.node_id = node_id
        self.current_term = 0
        self.voted_for = ""
        self.log: Log = Log()
        self.commit_index = 0
        self.current_term = 0
        self.cluster_membership: ClusterMembership = cluster_membership or ClusterMembership()
        self.state: State = Follower(self)

    @property
    def last_log_index(self):
        return self.log.last_index

    @property
    def last_log_term(self):
        return self.log[-1].term if self.log else 0  # pylint: disable=no-member

    def __setattr__(self, key, value):
        if (
            key == "current_term"
            and hasattr(self, "current_term")
            and self.current_term != value
        ):
            self.voted_for = ""
        object.__setattr__(self, key, value)

    def process_message(self, env: Envelope):
        if isinstance(env.msg, ClientMessage):
            return self.state.process_client_messsage(env.msg, env.source_node)
        if env.msg.term > self.current_term:
            self.increment_term(env.msg.term)
            self.transition_follower()
        return self.state.process_message(env.msg, env.source_node)

    def tick(self, ticks: int):
        self.state.tick(ticks)

    def transition_follower(self):
        self.state = Follower(self)

    def transition_candidate(self):
        self.state = Candidate(self)

    def transition_leader(self):
        self.state = Leader(self)

    def increment_term(self, new_term=None):
        self.voted_for = ""
        self.current_term = new_term or (self.current_term + 1)
